package imagebase64

import (
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime"
	"os"
	"path/filepath"
	"strings"
)

// EncodeFile encodes the image file at path to base64.
func EncodeFile(path string) (string, error) {
	file, err := os.Open(path)
	if err != nil {
		return "", err
	}
	contentType := mime.TypeByExtension(filepath.Ext(path))
	return Encode(file, contentType)
}

// Encode encodes the image data in r to base64.
func Encode(r io.Reader, contentType string) (string, error) {
	if !strings.HasPrefix(contentType, "image") {
		return "", errors.New("cannot encode contenttype " + contentType + ", this package encodes only images")
	}
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return "", fmt.Errorf("cannot encode: %w", err)
	}
	return "data:" + contentType + ";base64," + base64.StdEncoding.EncodeToString(data), nil
}

// Decode decodes the base64 encoded data in input and returns the bytes and content type.
func Decode(input string) (data []byte, contentType string, err error) {
	if !strings.HasPrefix(input, "data:") {
		err = fmt.Errorf("cannot decode: input string does not start with 'data:', and probably is not a base64 encoded image")
		return
	}
	pos := strings.Index(input, ";")
	if pos < 0 {
		err = fmt.Errorf("cannot decode: input string does not contain a content type, and probably is not a base64 encoded image")
		return
	}
	contentType = input[len("data:"):pos]
	if !strings.HasPrefix(contentType, "image") {
		err = fmt.Errorf("cannot decode: input string is not a base64 encoded image but " + contentType)
		return
	}
	pos2 := strings.Index(input, ",")
	encoding := input[pos+1 : pos2]
	if encoding != "base64" {
		err = fmt.Errorf("cannot decode: input string is not a base64 encoded image but " + encoding)
		return
	}
	data, err = base64.StdEncoding.Strict().DecodeString(input[pos2+1:])
	return
}
